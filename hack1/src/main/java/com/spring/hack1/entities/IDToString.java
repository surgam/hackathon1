package com.spring.hack1.entities;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
public class IDToString extends StdConverter<ObjectId,String> {
        @Override
        public String convert(ObjectId ID) {
                return ID.toString();
        }
    }
