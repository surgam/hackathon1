package com.spring.hack1.entities;

public enum TradeType {
    BUY("BUY"),
    SELL("SELL");
    private String value;

    private TradeType(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
