package com.spring.hack1.entities;

import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
public class StringToID extends StdConverter<String,ObjectId> {
        @Override
        public ObjectId convert(String ID) {
                return new ObjectId(ID);
        }
    }