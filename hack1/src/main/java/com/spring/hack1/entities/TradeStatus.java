package com.spring.hack1.entities;

public enum TradeStatus {
    CREATED("CREATED"), 
    PROCESSING("PROCESSING"),
    CANCELLED("CANCELLED"),
    REJECTED("REJECTED"),
    FULFILLED("FULFILLED"),
    PARTIALLY_FILLED("PARTIALLY_FILLED"),
    ERROR("ERROR");
    private String value;

    private TradeStatus(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}