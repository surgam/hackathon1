package com.spring.hack1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Holding {
    @Id
    @JsonIgnore
    private ObjectId id;
    @JsonIgnore
    private String tradeId;
    public String ticker;
    public int quantity;
    public double marketValue;

    public ObjectId get_id() {
        return id;
    }

    public void set_id(ObjectId id) {
        this.id = id;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(double marketValue) {
        this.marketValue = marketValue;
    }

    public Holding() { }
    public Holding(String ticker, int quantity, double marketValue){
        this.ticker = ticker;
        this.quantity = quantity;
        this.marketValue = marketValue;
    }

    public Holding(String ticker, String tradeId, int quantity, double marketValue){
        this.ticker = ticker;
        this.quantity = quantity;
        this.marketValue = marketValue;
        this.tradeId = tradeId;
    }

}
