package com.spring.hack1.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    @JsonSerialize(converter= IDToString.class)
    @JsonDeserialize(converter= StringToID.class)
    private ObjectId id;
    private Date date;
    private String ticker;
    private int quantity;
    private double reqPrice;
    private TradeStatus tradeStatus = TradeStatus.CREATED;
    private TradeType type;

    //ESTABLISH GETTER AND SETTER FOR GET ID AS STRING BECUASE OBJ ID NOT SERIALIZED (OBJ ID -> STRING AND VISE VERSA)
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getReqPrice() {
        return reqPrice;
    }

    public void setReqPrice(double reqPrice) {
        this.reqPrice = reqPrice;
    }

    public TradeStatus getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(TradeStatus tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }
}