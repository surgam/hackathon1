package com.spring.hack1.repo;

import com.spring.hack1.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepo extends MongoRepository<Trade, ObjectId> {
    
}