package com.spring.hack1.controllers;

import java.util.Collection;

import com.spring.hack1.entities.Trade;
import com.spring.hack1.entities.TradeStatus;
import com.spring.hack1.services.TradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trade")
public class TradeController {
    @Autowired
    private TradeService tradeService;
    
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades(){

        return tradeService.getAllTrades();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Trade getTrade( @PathVariable ("id") String id){

        return tradeService.getTrade(id);
    }

    @RequestMapping(path = "/status", method = RequestMethod.GET)
    public Collection<TradeStatus> getStatusOptions(){

        return tradeService.getStatusOptions();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Trade postTrade(@RequestBody Trade trade){

        return tradeService.addTrade(trade);
    }

    @RequestMapping (value = "/{id}",   method = RequestMethod.PUT)
    public Trade updateTradeStatus( @PathVariable ("id") String id, @RequestBody Trade trade){

        return tradeService.updateTrade(id, trade);
    }

    


}