package com.spring.hack1.controllers;

import java.util.List;

import com.spring.hack1.entities.Holding;
import com.spring.hack1.services.PortfolioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/trade/portfolio")
public class PortfolioController {

    @Autowired
    private PortfolioService service;
    
    @RequestMapping(method = RequestMethod.GET)
    public List<Holding> getPortfolioValue() {
        return service.getAllHoldings();
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    public List<Holding> getUserPortfolio(@PathVariable String userId){
        return service.getHoldingsByUser(userId);
    }
}
