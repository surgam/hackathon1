package com.spring.hack1.services;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.fields;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

import com.spring.hack1.entities.TradeType;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;

import java.util.*;
import java.util.stream.Collectors;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.internal.operation.AggregateOperation;
import com.spring.hack1.entities.Holding;
import com.spring.hack1.entities.Trade;
import com.spring.hack1.repo.PortfolioRepo;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PortfolioServiceImpl implements PortfolioService {

    private static final Logger LOG = LoggerFactory.getLogger(PortfolioServiceImpl.class);

    @Autowired
    private MongoTemplate template;

    @Autowired
    private PortfolioRepo repo;

    @Override
    public List<Holding> getAllHoldings() {
        String groupJson = "{ $group: { _id: \"$ticker\", quantity: { $sum: \"$quantity\" }, marketValue: {$sum: \"$marketValue\"} } }";
        String projectJson = "{$project: {_id: 0, ticker: \"$_id\", marketValue: 1, quantity: 1 }}";
        try {
            List<AggregationOperation> pipeline = createPipeline(groupJson, projectJson);
            Aggregation aggregation = newAggregation(pipeline);
            AggregationResults<Holding> results = template.aggregate(aggregation, Holding.class, Holding.class);
            return results.getMappedResults();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            LOG.error("Error: Receiving Portfolio Holdings", e);
        }
        return null;
    }


    public List<Holding> getAllHoldingsByTrades() {
        String matchJson = "{ \"$match\": {\"tradeStatus\" : \"FULFILLED\"}}";
        String groupJson = "{ \"$group\": { _id: \"$ticker\", quantity: { $sum: 1 }," +
                "marketValue: { $sum: { $multiply: [\"$reqPrice\", \"$quantity\"] } } }}";
        String projectJson = "{ \"$project\": { _id: 0, ticker: \"$_id\", marketValue: 1,  quantity: 1 }}";
        List<AggregationOperation> pipeline = createPipeline(matchJson, groupJson, projectJson);
        Aggregation aggregation = newAggregation(pipeline);
        try {
            AggregationResults<Holding> results = template.aggregate(aggregation, Trade.class, Holding.class);
            return results.getMappedResults();
        } catch (Exception e) {
            LOG.error("Error: Receiving Portfolio Holdings", e);
        }
        return null;
    }

    private AggregationOperation createAggregationOperation(String json) {
        Document document = Document.parse(json);
        return (ctx) -> ctx.getMappedObject(document);
    }

    private List<AggregationOperation> createPipeline(String ...stages){
        List<AggregationOperation> pipeline = new ArrayList<>();
        for (String stage: stages){
            pipeline.add(createAggregationOperation(stage));
        }
        return pipeline;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    private void loadTradesToPortfolioDb(){
        Query query = new Query().addCriteria(Criteria.where("tradeStatus").is("FULFILLED"));
        List<Trade> fulfilledTrades = template.find(query, Trade.class);
        List<Holding> holdings = repo.findAll();
        Set<String> tradeIds = new HashSet<>();
        for (Holding holding:holdings){
            tradeIds.add(holding.getTradeId());
        }
        List<Holding> newHoldings = new ArrayList<>();
        for (Trade trade: fulfilledTrades) {
            try {
                if (!tradeIds.contains(trade.getId().toHexString())){ if (trade.getType() == TradeType.BUY){
                        newHoldings.add(new Holding(trade.getTicker(), trade.getId().toHexString(), trade.getQuantity(),
                                trade.getQuantity() * trade.getReqPrice()));
                    } else if (trade.getType() == TradeType.SELL) {
                        newHoldings.add(new Holding(trade.getTicker(), trade.getId().toHexString(), -trade.getQuantity(),
                                (-1 * trade.getQuantity()) * trade.getReqPrice()));
                    }
                }
                repo.saveAll(newHoldings);
            } catch (IllegalArgumentException e) {
                LOG.error("Error: Finding With Trade ID:", e);
            }
        }
    }

    @Override
    public List<Holding> getHoldingsByUser(String userId) {
        return null;
    }
    
}
