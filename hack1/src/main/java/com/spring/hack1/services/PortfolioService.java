package com.spring.hack1.services;

import java.util.List;

import com.spring.hack1.entities.Holding;

public interface PortfolioService {
    List<Holding> getAllHoldings();
    List<Holding> getHoldingsByUser(String userId);
}
