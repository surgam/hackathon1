package com.spring.hack1.services;

import java.util.Collection;

import com.spring.hack1.entities.Trade;
import com.spring.hack1.entities.TradeStatus;

public interface TradeService {
    Trade addTrade(Trade t);
    Collection<Trade> getAllTrades();
    Trade updateTrade(String id, Trade trade);
    Collection<TradeStatus> getStatusOptions();  
    Trade getTrade(String id);
}