package com.spring.hack1.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import com.spring.hack1.entities.Trade;
import com.spring.hack1.entities.TradeStatus;
import com.spring.hack1.repo.TradeRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bson.types.ObjectId;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepo tradeRepo;

    @Override
    public Trade addTrade(Trade trade) {
        return tradeRepo.insert(trade);
    }

    @Override
    public Collection<Trade> getAllTrades() {
        return tradeRepo.findAll();
    }

    @Override
    public Trade updateTrade(String id, Trade trade) {
        ObjectId tradeId = new ObjectId(id);

        Optional<Trade> targetedTrade = tradeRepo.findById(tradeId);

        if (targetedTrade.isPresent() && targetedTrade.get().getTradeStatus() == TradeStatus.CREATED) {
            trade.setId(tradeId);
            return tradeRepo.save(trade);
        } else {
            return null;
        }
    }

    @Override
    public Collection<TradeStatus> getStatusOptions() {
        return Arrays.asList(TradeStatus.values()); 
    }
    

    @Override
    public Trade getTrade(String id) {

        ObjectId tradeId = new ObjectId(id);

        Optional<Trade> targetedTrade = tradeRepo.findById(tradeId);

        // If conditional below can be replaced by the following provided by Optional:
        // targetedTrade.getOrElse(null)
        
        if (targetedTrade.isPresent()) {
            return targetedTrade.get();
        } else {
            return null;
        }
    }
    
}