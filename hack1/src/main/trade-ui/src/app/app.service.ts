import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpClientModule,
  HttpHeaders,
} from '@angular/common/http';
import { Trade } from './portfolio/Trade';
import { Observable } from 'rxjs';

import * as API_KEY from '../../src/secret.json';

import { Quote } from './models/quote';
import { MarketNews } from './models/market-news';
import { StockRec } from './models/stock-rec';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  API_URL = '/api/trade';
  FINNHUB_URL = 'https://finnhub.io/api/v1/';
  API_KEY = API_KEY.API_KEY;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  constructor(private httpClient: HttpClient) {}

  public getTrades(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>('/api/trade/portfolio');
  }

  public getTradeHistory(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(`${this.API_URL}`);
  }

  // Finance API

  //GET single stock price
  public getStockQuote(ticker): Observable<Quote> {
    try {
      let results = this.httpClient.get<Quote>(
        `${this.FINNHUB_URL}quote?symbol=${ticker}&token=${this.API_KEY}`
      );
      console.log('Get Stock Quote', results);

      return results;
    } catch (error) {
      console.log('finnhub http e', error);
    }
  }

  public postTransaction(body) {
    return this.httpClient.post('/api/trade', body, this.httpOptions);
  }
  //GET market news

  public getMarketNews(): Observable<MarketNews[]> {
    let results = this.httpClient.get<MarketNews[]>(
      `${this.FINNHUB_URL}news?category=general&token=${this.API_KEY}`
    );
    console.log('Get Market News', results);

    return results;
  }

  //GET recommendation
  public getStockRecommendation(ticker): Observable<StockRec[]> {
    let results = this.httpClient.get<StockRec[]>(
      `${this.FINNHUB_URL}stock/recommendation?symbol=${ticker}&token=${this.API_KEY}`
    );
    console.log('Get Stock Recommendation', results);

    return results;
  }

  //GET single ETF price
  public getETF(ticker): Observable<Quote> {
    let results = this.httpClient.get<Quote>(
      `${this.FINNHUB_URL}quote?symbol=${ticker}&token=${this.API_KEY}`
    );
    console.log('Get Stock Quote', results);

    return results;
  }
}
