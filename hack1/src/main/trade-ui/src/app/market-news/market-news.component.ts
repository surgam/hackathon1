import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { logging } from 'protractor';
import { MarketNews } from '../models/market-news';

@Component({
  selector: 'app-market-news',
  templateUrl: './market-news.component.html',
  styleUrls: ['./market-news.component.css'],
})
export class MarketNewsComponent implements OnInit {
  constructor(private appService: AppService) {}

  marketNews: MarketNews[];

  ngOnInit() {
    this.getMarketNews();
  }
  public getMarketNews() {
    this.appService.getMarketNews().subscribe((res) => {
      console.log('response from api', res);

      this.marketNews = res;
    });
  }
}
