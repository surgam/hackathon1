import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { DataTablesModule } from 'angular-datatables';
import { ChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { TradingviewComponent } from './tradingview/tradingview.component';
import { AppComponent } from './app.component';
import { TradingViewProvider } from './providers';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { HistoryComponent } from './history/history.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { TransactionComponent } from './transaction/transaction.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarketNewsComponent } from './market-news/market-news.component';
import { StockRecommendationComponent } from './stock-recommendation/stock-recommendation.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { EtfIndexComponent } from './etf-index/etf-index.component';

@NgModule({
  declarations: [
    AppComponent,
    TradingviewComponent,
    PortfolioComponent,
    HistoryComponent,
    NavComponent,
    HomeComponent,
    TransactionComponent,
    MarketNewsComponent,
    StockRecommendationComponent,
    PieChartComponent,
    EtfIndexComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    ReactiveFormsModule,
    ChartsModule,
  ],
  providers: [TradingViewProvider],
  bootstrap: [AppComponent],
})
export class AppModule {}
