import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild,
  Inject,
  DoCheck,
  KeyValueDiffers,
  KeyValueDiffer,
} from '@angular/core';
import { TradingView } from '../providers';

@Component({
  selector: 'app-tradingview',
  templateUrl: './tradingview.component.html',
  styleUrls: ['./tradingview.component.css'],
})

/* Resources for implementation:
https://stackoverflow.com/questions/48173259/import-3rd-party-javascript-libraries-in-angular-single-components
https://www.tradingview.com/widget/fundamental-data/
https://www.tradingview.com/widget/advanced-chart/
https://angular.io/guide/architecture-services
https://stackoverflow.com/questions/48296351/embed-tradingview-into-angular-5
https://hackernoon.com/angular-providers-how-to-inject-3rd-party-library-af4a78722864
https://github.com/kobvel/ng-providers-common/blob/3rd-party-lib-injection/src/app/app.module.ts
*/
export class TradingviewComponent implements AfterViewInit, DoCheck {
  // allows for loading with any symbol
  @Input() symbol: string = '';
  settings: any = {};
  // id for being able to check for errors using postMessage
  widgetId: string = '';
  differ: any;
  // wanted to be able to hide the widget if the symbol passed in was invalid (don't love their sad cloud face)
  @ViewChild('containerDiv', { static: false }) containerDiv: ElementRef;

  constructor(
    @Inject(TradingView) private client,
    private differs: KeyValueDiffers
  ) {
    this.differ = this.differs.find({}).create();
  }

  ngDoCheck() {
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem((item) => {
        console.log('item changed', item);
      });
    }
  }

  ngAfterViewInit() {
    //  need to do this in AfterViewInit because of the Input
    setTimeout(() => {
      this.settings = {
        symbol: this.symbol,
        colorTheme: 'Dark',
        isTransparent: false,
        largeChartUrl: '',
        displayMode: 'regular',
        autosize: false,
        locale: 'en',
        allow_symbol_change: true,
        container_id: 'tradingview-widget-container__widget',
        interval: 'D',
        timezone: 'Etc/UTC',
        style: '1',
        toolbar_bg: 'rgba(0, 0, 0, 1)',
        enable_publishing: false,
        height: '500px',
        width: '1110',
      };
      new this.client.widget(this.settings);
    });
    console.log(this.symbol);
  }
}
