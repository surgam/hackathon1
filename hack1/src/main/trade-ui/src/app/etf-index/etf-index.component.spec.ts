import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtfIndexComponent } from './etf-index.component';

describe('EtfIndexComponent', () => {
  let component: EtfIndexComponent;
  let fixture: ComponentFixture<EtfIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtfIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtfIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
