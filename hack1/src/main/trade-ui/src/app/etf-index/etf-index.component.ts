import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Quote } from '../models/quote';
import { logging } from 'protractor';

@Component({
  selector: 'app-etf-index',
  templateUrl: './etf-index.component.html',
  styleUrls: ['./etf-index.component.css'],
})
export class EtfIndexComponent implements OnInit {
  constructor(private appService: AppService) {}

  etf1: string = 'SPY'; //0
  etf2: string = 'DIA'; //1
  etf3: string = 'QQQ'; //2

  apiETFQuote: any = [];

  ngOnInit() {
    this.getQuote(this.etf1);
    this.getQuote(this.etf2);
    this.getQuote(this.etf3);
  }
  public getQuote(ticker: string) {
    this.appService.getStockQuote(ticker).subscribe((res) => {
      console.log('response from api', res);

      this.apiETFQuote.push(res);
    });
  }
}
