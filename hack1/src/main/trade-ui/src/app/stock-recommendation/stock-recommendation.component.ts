import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { logging } from 'protractor';
import { StockRec } from '../models/stock-rec';
import { DataService } from '../data.service';

@Component({
  selector: 'app-stock-recommendation',
  templateUrl: './stock-recommendation.component.html',
  styleUrls: ['./stock-recommendation.component.css'],
})
export class StockRecommendationComponent implements OnInit {
  stockRecommendation: StockRec[];
  constructor(
    private appService: AppService,
    private dataService: DataService
  ) {}

  ticker: string = 'AAPL';

  ngOnInit() {
    this.getStockRecommendation(this.ticker);
    this.dataService.currentMessage.subscribe((message) =>
      this.getStockRecommendation(message)
    );
  }
  public getStockRecommendation(ticker) {
    this.appService.getStockRecommendation(ticker).subscribe((res) => {
      console.log('response from api', res);

      this.stockRecommendation = res;
    });
  }
}
