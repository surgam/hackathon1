import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Observable, Subject, interval, Subscription } from 'rxjs';

import { AppService } from '../app.service';
import { Trade } from './Trade';


@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  
  trades = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  private updatedSubscription: Subscription;
  
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.getTrades();
  }
 
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
 }
  
  rerender(): void {
    console.log("refreshed!");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) =>{
    dtInstance.destroy();
    this.getTrades();

    });
  }
 
  public getTrades() {
      
      this.appService.getTrades().subscribe(data=>{
      this.trades = data.filter((data) => data["quantity"] > 0);
      
      this.dtTrigger.next();
    }) 
      
  }
  

}
