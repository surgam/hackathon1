export class Trade{
    date: Date;
    ticker: string;
    qty: number;
    price: number;
    tradeStatus: string;
    marketValue: string;
    tradeType: string; 
    
}