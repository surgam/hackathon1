import { InjectionToken, ValueProvider } from '@angular/core';

interface ITradingView {
  widget: (settings: any) => any;
}

const TradingView: InjectionToken<ITradingView> = new InjectionToken<ITradingView>('TradingView');
const TradingViewProvider: ValueProvider = { provide: TradingView, useValue: window['TradingView'] };

export { ITradingView, TradingView, TradingViewProvider };