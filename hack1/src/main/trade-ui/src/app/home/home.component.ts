import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Quote } from '../models/quote';
import { logging } from 'protractor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private appService: AppService) {}

  // ngOnInit(): void {
  // }

  apiQuote: Quote;
  ticker: string = 'QQQ';

  ngOnInit() {
    this.getQuote(this.ticker);
  }
  public getQuote(ticker: string) {
    this.appService.getStockQuote(ticker).subscribe((res) => {
      console.log('response from api', res);

      this.apiQuote = res;
    });
  }
}
