import {
  Component,
  Input,
  AfterViewInit,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import {
  SingleDataSet,
  Label,
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip,
} from 'ng2-charts';
import { Trade } from '../portfolio/Trade';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css'],
})
export class PieChartComponent implements OnChanges {
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  @Input() public trades: Trade[] = [];
  public pieChartLabels: Label[] = [];
  @Input() public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(110, 114, 20, 1)',
        'rgba(118, 183, 172, 1)',
        'rgba(0, 148, 97, 1)',
        'rgba(129, 78, 40, 1)',
        'rgba(129, 199, 111, 1)',
      ],
    },
  ];
  constructor() {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.trades);
    this.pieChartLabels = this.trades
      .filter((trade) => Number(trade.marketValue) > 0)
      .map((trade) => trade.ticker);
    this.pieChartData = this.trades
      .map((trade) => Number(trade.marketValue))
      .filter((value) => value > 0);
  }
}
