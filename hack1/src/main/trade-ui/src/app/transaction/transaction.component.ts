import { Quote } from '../models/quote';
import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppService } from '../app.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
})
export class TransactionComponent implements OnInit {
  
  apiQuote: Quote;
  ticker: string;
  finalTicker: string;
  quantity: number = 1;
  finalQuantity: number;
  frmRegister: FormGroup;
  text: String;
  price: number = 0.0;
  total: number = 0.0;
  type: String;
  data: any;
  trades = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private _fb: FormBuilder,
    private appService: AppService,
    private eRef: ElementRef,
    private router: Router,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  public createForm() {
    this.frmRegister = this._fb.group({
      regTicker: '',
      regQuantity: this.quantity,
      regPrice: this.price,
    });
  }

  public postTransaction(value) { 
    if(this.apiQuote['c'] == 0){
      alert("Please enter a valid ticker");
      this.createForm();
    }
    else{
      this.type = document.activeElement.getAttribute("Name");
      this.finalTicker = value.regTicker; 
      this.finalQuantity = value.regQuantity;
      console.log(this.price)
      this.data = (JSON.stringify({ 
        date: new Date(),
        ticker: this.finalTicker,
        quantity: this.finalQuantity,
        reqPrice: this.price,
        type: this.type 
      }));
      if(this.frmRegister.valid){
        if(confirm("Are you sure you want to "+this.type)){
          this.appService.postTransaction(this.data).subscribe((res) => {
            console.log('response from post', res);
          });
          console.log("Form Submitted!");
          location.reload();
        }
        else{
          this.createForm()     
        }
      }
      
    }   
  }

  public getQuote(ticker: string) {
    this.appService.getStockQuote(ticker).subscribe((res) => {
      console.log('response from api', res);

      this.apiQuote = res;
      console.log(this.apiQuote);
      this.setPrice();
    });
  }
  public setPrice() {
    if (this.apiQuote === undefined) {
      this.price = 0;
    } else {
      this.price = this.apiQuote['c'];
    }
  }
  onQtyChange(event) {
    this.quantity = event;
    this.total = this.quantity * this.apiQuote['c'];
  }
  onTickerChange(event) {
    this.ticker = event;
    this.getQuote(this.ticker);
    this.dataService.changeMessage(this.ticker);
    
  }
}
