import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { interval, Observable, Subject, Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { Trade } from '../portfolio/Trade';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  trades = [];
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  private updatedSubscription: Subscription;
  
  constructor(private appService: AppService) { }

  ngOnInit() {
    
    this.getTrades();
    /*this.updatedSubscription = interval(10000).subscribe(
      (val) => {this.rerender() }    
  );*/
    
  }
  rerender(): void {
    console.log("refreshed!");
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) =>{
    dtInstance.destroy();
    this.getTrades();
    
      
    });
  }
  public getTrades() {
      this.appService.getTradeHistory().subscribe(data=>{
      this.trades = data;
      this.dtTrigger.next();
      
    })  
     
  }

}
