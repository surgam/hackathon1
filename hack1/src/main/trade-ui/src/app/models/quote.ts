export interface Quote {
  c: number; //CLOSE
  h: number; //HIGH
  l: number; //LOW
  o: number; //OPEN
  pc: number; //Previous Close Price
  t: number; // Timestamp
}
