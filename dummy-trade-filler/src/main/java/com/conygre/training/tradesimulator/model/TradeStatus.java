package com.conygre.training.tradesimulator.model;

public enum TradeStatus {
    CREATED("CREATED"), 
    PROCESSING("PROCESSING"),
    CANCELLED("CANCELLED"),
    REJECTED("REJECTED"),
    FULFILLED("FULFILLED"),
    PARTIALLY_FILLED("PARTIALLY_FILLED"),
    ERROR("ERROR");
    private String value;

    private TradeStatus(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

// public enum TradeState {
//     CREATED("CREATED"),
//     PROCESSING("PROCESSING"),
//     FILLED("FILLED"),
//     REJECTED("REJECTED");

//     private String state;

//     private TradeState(String state) {
//         this.state = state;
//     }

//     public String getState() {
//         return this.state;
//     } 
// }
