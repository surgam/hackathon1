package com.conygre.training.tradesimulator.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;
    private Date date;
    private String ticker;
    private int quantity;
    private double reqPrice;
    private TradeStatus tradeStatus = TradeStatus.CREATED;
    private TradeType type;

    //ESTABLISH GETTER AND SETTER FOR GET ID AS STRING BECUASE OBJ ID NOT SERIALIZED (OBJ ID -> STRING AND VISE VERSA)
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getReqPrice() {
        return reqPrice;
    }

    public void setReqPrice(double reqPrice) {
        this.reqPrice = reqPrice;
    }

    public TradeStatus getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(TradeStatus tradeStatus) {
        this.tradeStatus = tradeStatus;
    }
    
    public TradeType getTradeType() {
        return type;
    }

    public void setTradeType(TradeType type) {
        this.type = type;
    }
    
}


