package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<Trade> findTradesForProcessing(){
        List<Trade> foundTrades = tradeDao.findByTradeStatus(TradeStatus.CREATED);

        for(Trade thisTrade: foundTrades) {
            thisTrade.setTradeStatus(TradeStatus.PROCESSING);
            tradeDao.save(thisTrade);
        }

        System.out.println("Inside findTradesForProcessing");
        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(){
        List<Trade> foundTrades = tradeDao.findByTradeStatus(TradeStatus.PROCESSING);

        for(Trade thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 8) {
                thisTrade.setTradeStatus(TradeStatus.REJECTED);
            }
            else {
                thisTrade.setTradeStatus(TradeStatus.FULFILLED);
            }
            tradeDao.save(thisTrade);
        }

        System.out.println("Inside findTradesForFilling");
        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {

        // System.out.println("Testing");

        LOG.info("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.info("Found " + tradesForFilling + " trades to be filled/rejected");

        // System.out.println("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.info("Found " + tradesForProcessing + " trades to be processed");

        // System.out.println("Found " + tradesForProcessing + " trades to be processed");

    }

    // @Scheduled(fixedRate = 1000)
    // public void testSim () {
    //     System.out.println("Hellop");
    // }
}
